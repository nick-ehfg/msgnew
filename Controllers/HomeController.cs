﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace MedSuppGuideNew.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "About MedSuppGuide";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Contact Us Here";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Video()
        {

            ViewData["Message"] = "Check Out our Resources and Videos";

            return View();

        }


        public IActionResult PrivacyPolicy()
        {

            ViewData["Message"] = "Privacy Policy";

            return View();

        }

        public IActionResult Result()
        {

            ViewData["Message"] = "Result";

            return View();

        }



        //QUOTES START BELOW
        public IActionResult QuoteResults()
        {
            ViewData["Message"] = "Run a Free Medicare Supplement Quote Now";

            return View();
        }

        public IActionResult QuoteStart()
        {
            ViewData["Message"] = "Start Quote Here";

            return View(); 
        }



    }
}
