﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedSuppGuideNew.Models
{
    public class VideoModel
    {
        public int VideoId { get; set; }
        public string VideoTitle { get; set; }

        public string Url { get; set; }

        public DateTime DatePublished { get; set; }
        public string AuthorAccount { get; set; }

        public string AccountUrl { get; set; }
    }
}
